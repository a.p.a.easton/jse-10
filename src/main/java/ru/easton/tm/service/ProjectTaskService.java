package ru.easton.tm.service;

import ru.easton.tm.entity.Project;
import ru.easton.tm.entity.Task;
import ru.easton.tm.repository.ProjectRepository;
import ru.easton.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if(projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task addTaskToProject(final Long projectId, final Long taskId){
        if(projectId == null) return null;
        if(taskId == null) return null;
        final Project project = projectRepository.findById(projectId);
        if(project == null) return null;
        final Task task = taskRepository.findById(taskId);
        if(task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId){
        if(projectId == null) return null;
        if(taskId == null) return null;
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if(task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
